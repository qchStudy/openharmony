#ifndef  __LED_PWM_H__
#define __LED_PWM_H__

#include "iot_base_type.h"

/* 初始化LED模块 */
u32 LedPwmInit(u32 pin);
void LedPwmCtrl(unsigned int status);

#endif

