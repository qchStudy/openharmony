#include <stdio.h>
#include "ohos_init.h"
#include "cmsis_os2.h"
#include "iot_pwm.h"
#include "wifiiot_gpio.h"
#include "iot_gpio.h"
#include "iot_gpio_ex.h"
#include "led_pwm.h"

#define LED_FREQ 64000
#define LED_DUTY 100

static unsigned int spwmPort = 0;


/* 初始化LED模块 */
u32 LedPwmInit(u32 pin)
{
    /* LED灯根据输入参数GPIO引脚初始化 */
    u32 ret = IoTGpioInit(pin);
	if(ret != 0){ 
        printf("IoTGpioInit failed :%#x \r\n",ret);
		return ret;
    }

    /* 根据引脚号确定引脚PWM功能配置值及PWM端口号 */
    u8 pinFunc = 0;
    switch (pin)
    {
        case WIFI_IOT_GPIO_IDX_0:
            pinFunc = IOT_GPIO_FUNC_GPIO_0_PWM3_OUT;
            spwmPort = PWM_PORT_PWM3;
            break;
        case WIFI_IOT_GPIO_IDX_1:
            pinFunc = IOT_GPIO_FUNC_GPIO_1_PWM4_OUT;
            spwmPort = PWM_PORT_PWM4;
            break;
        case WIFI_IOT_GPIO_IDX_2:
            pinFunc = IOT_GPIO_FUNC_GPIO_2_PWM2_OUT;
            spwmPort = PWM_PORT_PWM2;
            break;
        case WIFI_IOT_GPIO_IDX_3:
            pinFunc = IOT_GPIO_FUNC_GPIO_3_PWM5_OUT;
            spwmPort = PWM_PORT_PWM5;
            break;
        case WIFI_IOT_GPIO_IDX_4:
            pinFunc = IOT_GPIO_FUNC_GPIO_4_PWM1_OUT;
            spwmPort = PWM_PORT_PWM1;
            break;
        case WIFI_IOT_GPIO_IDX_5:
            pinFunc = IOT_GPIO_FUNC_GPIO_5_PWM2_OUT;
            spwmPort = PWM_PORT_PWM2;
            break;
        case WIFI_IOT_GPIO_IDX_6:
            pinFunc = IOT_GPIO_FUNC_GPIO_6_PWM3_OUT;
            spwmPort = PWM_PORT_PWM3;
            break;
        case WIFI_IOT_GPIO_IDX_7:
            pinFunc = IOT_GPIO_FUNC_GPIO_7_PWM0_OUT;
            spwmPort = PWM_PORT_PWM0;
            break;
        case WIFI_IOT_GPIO_IDX_8:
            pinFunc = IOT_GPIO_FUNC_GPIO_8_PWM1_OUT;
            spwmPort = PWM_PORT_PWM1;
            break;
        case WIFI_IOT_GPIO_IDX_9:
            pinFunc = IOT_GPIO_FUNC_GPIO_9_PWM0_OUT;
            spwmPort = PWM_PORT_PWM0;
            break;
        case WIFI_IOT_GPIO_IDX_10:
            pinFunc = IOT_GPIO_FUNC_GPIO_10_PWM1_OUT;
            spwmPort = PWM_PORT_PWM1;
            break;
        case WIFI_IOT_GPIO_IDX_11:
            pinFunc = IOT_GPIO_FUNC_GPIO_11_PWM2_OUT;
            spwmPort = PWM_PORT_PWM2;
            break;
        case WIFI_IOT_GPIO_IDX_12:
            pinFunc = IOT_GPIO_FUNC_GPIO_12_PWM3_OUT;
            spwmPort = PWM_PORT_PWM3;
            break;
        case WIFI_IOT_GPIO_IDX_13:
            pinFunc = IOT_GPIO_FUNC_GPIO_13_PWM4_OUT;
            spwmPort = PWM_PORT_PWM4;
            break;
        case WIFI_IOT_GPIO_IDX_14:
            pinFunc = IOT_GPIO_FUNC_GPIO_14_PWM5_OUT;
            spwmPort = PWM_PORT_PWM5;
            break;
        default:
            return KP_ERR_FAILURE;
    }

	/* 设置复用为PWM功能 */
	ret = IoTGpioSetFunc(pin, pinFunc);
	if(ret != 0){ 
        printf("IoTGpioSetFunc failed :%#x \r\n",ret);
		return ret;
    }

	/* PWM初始化 */
    ret = IoTPwmDeinit(spwmPort);
    if(ret != 0){ 
        printf("IoTPwmDeinit failed :%#x \r\n",ret);
		return ret;
    }

    ret = IoTPwmInit(spwmPort);
    if(ret != 0){ 
        printf("IoTPwmInit failed :%#x \r\n",ret);
		return ret;
    }
}

void LedPwmCtrl(unsigned int status)
{
	switch (status) {
	case LED_ON:
	    IoTPwmStart(spwmPort, LED_DUTY, LED_FREQ);
		break;
	case LED_OFF:
		IoTPwmStop(spwmPort);
		break;
	case LED_SPARK:
		IoTPwmStart(spwmPort, LED_DUTY, LED_FREQ);
		osDelay(LED_SPARK_INTERVAL_TIME);
		IoTPwmStop(spwmPort);
		osDelay(LED_SPARK_INTERVAL_TIME);
		break;
	default:
		osDelay(LED_SPARK_INTERVAL_TIME);
		break;
    }

    return;
}

