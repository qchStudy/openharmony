#ifndef __LED_GPIO_H__
#define __LED_GPIO_H__

#include "iot_base_type.h"


//初始化烟雾探测模块的LED报警灯
void SafeguardLedInit(void);

//控制烟雾探测模块的LED报警灯，1:点亮; 2:熄灭
void SafeguardLedCtrl(u32 state);


#endif

