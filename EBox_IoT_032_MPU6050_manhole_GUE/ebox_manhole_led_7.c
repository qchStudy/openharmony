#include "iot_gpio.h"
#include "iot_gpio_ex.h"
#include "ebox_manhole_led_7.h"

#define LED_IO_GPIO_07  7

#define LED_OFF 0
#define LED_ON 1
#define LED_SPARK 2

/* 按照注释要求补全函数功能代码 */
/* 初始化LED */
void ManholeLedInit(void)
{
	/* LED报警灯连接芯片的GPIO7引脚，初始化GPIO7引脚 */
    int ret = IoTGpioInit(LED_IO_GPIO_07);
    if (ret != 0)
    {
        printf("IoTGpioInit failed :%#x \r\n", ret);
        return;
    }

	/* 将GPIO7引脚复用为GPIO功能 */
	 ret = IoTGpioSetFunc(LED_IO_GPIO_07, IOT_GPIO_FUNC_GPIO_7_GPIO);
    if (ret != 0)
    {
        printf("IoTGpioSetFunc failed :%#x \r\n", ret);
        return;
    }

	/* 将GPIO7引脚设置为输出有效 */
	ret = IoTGpioSetDir(LED_IO_GPIO_07, IOT_GPIO_DIR_OUT);
    if (ret != 0)
    {
        printf("IoTGpioSetDir failed :%#x \r\n", ret);
        return;
    }
	
	printf("ManholeLedInit success!\r\n");
}

/* 按照注释要求补全函数功能代码 */
/* 根据入参state点亮或熄灭LED灯 */
void ManholeLedCtrl(u32 state)
{
	switch (state)
    {
    case LED_ON:
        /* code */IoTGpioSetOutputVal(LED_IO_GPIO_07, LED_ON);
        break;
    
    default:
        IoTGpioSetOutputVal(LED_IO_GPIO_07, LED_OFF);
        break;
	}
}

