#ifndef  __IOT_GPIO_IIC_H__
#define __IOT_GPIO_IIC_H__

#include "iot_base_type.h"


/* GPIO初始化复用为IIC功能 */
u32 GpioInitIIC(u32 pinSDA, u32 pinSCL, u32 baud);



#endif

