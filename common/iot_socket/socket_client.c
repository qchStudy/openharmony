
/*
 * Copyright (c) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "socket_client.h"

#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <netinet/in.h>
#include <cmsis_os2.h>
#include <lwip/inet.h>
#include <lwip/netdb.h>
#include <sys/socket.h>
#include <sys/time.h>

#include "ohos_types.h"

#define    SOCKET_TASK_STACK    (1024 * 8)
#define    SOCKET_TASK_PERIOD    27

#define    UDP_DEF_PORT        9090
#define    TCP_DEF_PORT        8989

#define    MSG_DATE_BUF_LEN    24
#define    PROTOCOL_HEADER        "HM"
#define    DATA_DEVICE_NAME    "TD"
#define    DATA_SWITCH         "TA"
#define    DATA_MSG_ON         "on"
#define    DATA_MSG_OFF        "off"

#ifndef    ARRAYSIZE
#define    ARRAYSIZE(a)    (sizeof((a)) / sizeof((a[0])))
#endif
#ifndef    bool
#define    bool    unsigned char
#endif
#ifndef    true
#define    true    1
#endif
#ifndef    false
#define    false    0
#endif

#define  SOCKET_CLIENT_DEBUG
#ifdef  SOCKET_CLIENT_DEBUG
#define SCK_ERR(fmt, args...)   printf("[SOCKET_ERROR][%s|%d]" fmt, __func__, __LINE__, ##args)
#define SCK_DBG(fmt, args...)   printf("[SOCKET_DEBUG][%s|%d]" fmt, __func__, __LINE__, ##args)
#define SCK_INFO(fmt, args...)   printf("[SOCKET_INFO][%s|%d]" fmt, __func__, __LINE__, ##args)
#else
#define SCK_ERR(fmt, args...)   do {} while(0)
#define SCK_DBG(fmt, args...)   do {} while(0)
#define SCK_INFO(fmt, args...)  do {} while(0)
#endif

static void ResolveDevName(SocketEventCallback callback, char *value);
static void ResolveSwitch(SocketEventCallback callback, char *value);

typedef union {
    char msg[MSG_DATE_BUF_LEN];
    struct {
        char head[2];
        char cmd[2];
        char buff[20];
    } msg_info;
} MsgInfo;

typedef struct {
    char cmd[MSG_DATE_BUF_LEN];
    void (*func)(SocketEventCallback callback, char *value);
} MsgData;

static MsgData g_msgData[] = {
    {DATA_DEVICE_NAME, ResolveDevName},
    {DATA_SWITCH, ResolveSwitch}
};

static bool g_threadRunning = false;

// ********************************************************************************************************************************** //
static bool IsEqualTo(const char *msg1, const char *msg2, int length)
{
    if (msg1 == NULL || msg2 == NULL || length <= 0) {
        SCK_ERR("NULL POINT! \n");
        return false;
    }

    return (strncasecmp(msg1, msg2, length) == 0);
}

static void ResolveDevName(SocketEventCallback callback, char *value)
{
    SCK_INFO(" ########### value : %s ################ \n", value);
}

static void ResolveSwitch(SocketEventCallback callback, char *value)
{
    SCK_INFO(" ########### value : %s ################ \n", value);
    if (callback != NULL) {
        callback(SOCKET_SET_CMD, value);
    }
}

static int SocketClientResolveData(const char *data, int len, SocketEventCallback callback)
{
    MsgInfo msgInfo = {0};
    if (data == NULL || len <= 0) {
        SCK_ERR("NULL POINT!\n");
        return -1;
    }

    if (len > MSG_DATE_BUF_LEN) {
        len = MSG_DATE_BUF_LEN;
    }

    memcpy(msgInfo.msg, data, len);
    SCK_DBG("head:%s\n", msgInfo.msg_info.head);
    for (int i = 0; i < ARRAYSIZE(g_msgData); i++) {
        if (IsEqualTo(msgInfo.msg_info.cmd, g_msgData[i].cmd, strlen(g_msgData[i].cmd))) {
            g_msgData[i].func(callback, msgInfo.msg_info.buff);
            SCK_INFO(" cmd %s is match! \n", g_msgData[i].cmd);
            break;
        }
    }

    return 0;
}

static int SocketOpen(const char *ip, int port)
{
    int sockfd;
    struct sockaddr_in recvAddr;
    if (ip == NULL || port <= 0) {
        return -1;
    }

    sockfd = socket(AF_INET, SOCK_STREAM, 0);
    if (sockfd < 0) {
        SCK_ERR("socket failed! errno=%d\n", errno);
        return -1;
    }

    memset(&recvAddr, 0x00, sizeof(recvAddr));
    recvAddr.sin_family = AF_INET;
    recvAddr.sin_port = htons(port);
    recvAddr.sin_addr.s_addr = inet_addr(ip);

    if (connect(sockfd, (struct sockaddr *)&recvAddr, sizeof(recvAddr)) < 0) {
        SCK_ERR("connect failed! errno=%d\n", errno);
        close(sockfd);
        return -1;
    }

    return sockfd;
}

static int GetServerIp(char *ip, int size)
{
    char recMsg[256] = {0};
    char sockfd;
    char *tmp = NULL;
    struct sockaddr_in localAddr, serverAddr;
    int sockaddr_len = sizeof(serverAddr);

    sockfd = socket(AF_INET, SOCK_DGRAM, 0);
    if (sockfd < 0) {
        SCK_ERR("socket failed!\n");
        return -1;
    }

    memset(&localAddr, 0x00, sizeof(localAddr));
    localAddr.sin_family = AF_INET;
    localAddr.sin_port = htons(UDP_DEF_PORT);
    localAddr.sin_addr.s_addr = inet_addr(INADDR_ANY);

    if (bind(sockfd, (struct sockaddr *)&localAddr, sizeof(localAddr)) < 0) {
        SCK_ERR("bind failed! errno : %d(%s)\n", errno, strerror(errno));
        close(sockfd);
        return -1;
    }

    if (recvfrom(sockfd, recMsg, sizeof(recMsg), 0, (struct sockaddr *)&serverAddr, &sockaddr_len) < 0) {
        SCK_ERR("recvfrom failed! \n");
        close(sockfd);
        return -1;
    }

    tmp = inet_ntoa(serverAddr.sin_addr);
    SCK_INFO("the server ip is %s \n", tmp);
    if (ip == NULL || size < strlen(tmp)) {
        SCK_ERR("params is invalid!! \n");
        close(sockfd);
        return -1;
    }

    strncpy(ip, tmp, strlen(tmp));

    close(sockfd);

    return 0;
}

static void SocketClientProcess(void *arg)
{
    int sockfd = -1;
    char ipBuf[256] = {0};
    char sendBuf[256] = {0};
    char mDeviceName[256] = {0};

    SocketCallback *mCallback = (SocketCallback *)arg;
    if (mCallback == NULL) {
        SCK_ERR("socket callback is NULL! \n");
        return;
    }

    if (GetServerIp(ipBuf, sizeof(ipBuf)) < 0) {
        SCK_ERR("get server ip failed! \n");
        g_threadRunning = false;
        return;
    }

    sockfd = SocketOpen((const char *)ipBuf, TCP_DEF_PORT);
    if (sockfd < 0) {
        SCK_ERR("socket open failed! \n");
        g_threadRunning = false;
        return;
    }

    if (mCallback->socketEvent != NULL) {
        mCallback->socketEvent(SOCKET_CONNECTTED, NULL);
    }

    if (mCallback->socketGetDeviceName != NULL) {
        mCallback->socketGetDeviceName(mDeviceName, sizeof(mDeviceName));
        sprintf(sendBuf, "%s%s%s", PROTOCOL_HEADER, DATA_DEVICE_NAME, mDeviceName);
        if (send(sockfd, sendBuf, strlen(sendBuf), 0) < 0) {
            SCK_ERR("send %s failed! \n", sendBuf);
            goto EXIT;
        }
    }

    while (g_threadRunning) {
        char recvBuf[1024] = {0};
        int recvBytes = recv(sockfd, recvBuf, sizeof(recvBuf), 0);
        if (recvBytes <= 0) {
            break;
        }
        SCK_INFO("recvMsg[%d] : %s \n", recvBytes, recvBuf);
        if (SocketClientResolveData((const char *)recvBuf, recvBytes, mCallback->socketEvent) < 0) {
            break;
        }
    }

EXIT:
    close(sockfd);
    sockfd = -1;
    if (mCallback->socketEvent != NULL) {
        mCallback->socketEvent(SOCKET_DISCONNECT, NULL);
    }
    g_threadRunning = false;
}

int SocketClientStart(SocketCallback *gCallback)
{
    osThreadAttr_t attr;

    attr.name = "SocketClientTask";
    attr.attr_bits = 0U;
    attr.cb_mem = NULL;
    attr.cb_size = 0U;
    attr.stack_mem = NULL;
    attr.stack_size = SOCKET_TASK_STACK;
    attr.priority = SOCKET_TASK_PERIOD;

    g_threadRunning = true;

    if (osThreadNew(SocketClientProcess, (void *)gCallback, &attr) == NULL) {
        SCK_ERR("Falied to create ClientTask!\n");
        return -1;
    }

    return 0;
}

void SocketClientStop(void)
{
    if (g_threadRunning) {
        g_threadRunning = false;
    }
}
