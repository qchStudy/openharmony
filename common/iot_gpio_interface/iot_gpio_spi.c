#include "wifiiot_gpio.h"
#include "iot_gpio.h"
#include "iot_gpio_ex.h"
#include "iot_spi.h"
#include "iot_gpio_spi.h"
#include "iot_gpio_func.h"


/* GPIO初始化复用为SPI功能，pinRES为SPI复位引脚，pinRXD为SPI的数据/命令控制引脚，pinCS为SPI的片选引脚 */
u32 GpioInitSpi(u32 pinRES, u32 pinRXD, u32 pinCS)
{
    /* 根据输入参数GPIO引脚号初始化 */
    u32 ret = KP_ERR_SUCCESS;
   
    // 连接SPI的RES引脚，负责复位SPI
    ret = IoTGpioInit(pinRES);
    if(ret != KP_ERR_SUCCESS){ 
        IOT_PRINTF_FUNC("IoTGpioInit failed :%#x \r\n", ret);
        return ret;
    }
    
    /* 根据引脚号确定引脚GPIO功能配置值 */
    u8 resFunc = 0;
    ret = Gpio2Gpio(pinRES, &resFunc);
    if(ret != KP_ERR_SUCCESS){ 
        IOT_PRINTF_FUNC("Gpio2Gpio failed :%#x \r\n", ret);
        return ret;
    }
    
    /* 设置复用为GPIO功能 */
    ret = IoTGpioSetFunc(pinRES, resFunc);
    if(ret != KP_ERR_SUCCESS){ 
        printf("IoTGpioSetFunc failed :%#x \r\n",ret);
        return ret;
    }

    /* 根据引脚号确定引脚SPI功能的指令控制DC端口号 */
    u8 rxdFunc = 0;
    ret = Gpio2Gpio(pinRXD, &rxdFunc);
    if(ret != KP_ERR_SUCCESS){ 
        IOT_PRINTF_FUNC("Gpio2Gpio failed :%#x \r\n", ret);
        return ret;
    }
    
    /* 连接设备的DC指令控制引脚，负责对设备进行指令控制，设置复用为GPIO功能 */
    ret = IoTGpioSetFunc(pinRXD, rxdFunc);
    if(ret != KP_ERR_SUCCESS){ 
        printf("IoTGpioSetFunc failed :%#x \r\n",ret);
        return ret;
    }

    /* 设置为输出控制 */
    ret = IoTGpioSetDir(pinRXD, IOT_GPIO_DIR_OUT);
    if (ret != KP_ERR_SUCCESS) {
        printf("IoTGpioSetDir failed :%#x \r\n",ret); 
        return;
    }
    
    /* 根据引脚号确定引脚SPI功能的指令控制CS端口号 */
    u8 csFunc = 0;
    ret = Gpio2Gpio(pinCS, &csFunc);
    if(ret != KP_ERR_SUCCESS){ 
        IOT_PRINTF_FUNC("Gpio2Gpio failed :%#x \r\n", ret);
        return ret;
    }
    
    //连接设备的CS片选引脚，负责使能设备，设置复用为GPIO功能
    ret = IoTGpioSetFunc(pinCS, csFunc);
    if(ret != KP_ERR_SUCCESS){ 
        printf("IoTGpioSetFunc failed :%#x \r\n",ret);
        return ret;
    }

    /* 设置为输出控制 */
    ret = IoTGpioSetDir(pinCS, IOT_GPIO_DIR_OUT);
    if (ret != KP_ERR_SUCCESS) {
        printf("IoTGpioSetDir failed :%#x \r\n",ret); 
        return ret;
    }
    
    return KP_ERR_SUCCESS;
}

