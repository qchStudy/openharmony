#include "wifiiot_gpio.h"
#include "iot_gpio.h"
#include "iot_gpio_ex.h"
#include "iot_i2c.h"
#include "iot_gpio_i2c.h"
#include "iot_gpio_func.h"


/* GPIO初始化复用为IIC功能 */
u32 GpioInitIIC(u32 pinSDA, u32 pinSCL, u32 baud)
{
    /* 根据输入参数GPIO引脚号初始化 */
    u32 ret = IoTGpioInit(pinSDA);
    if(ret != KP_ERR_SUCCESS){ 
        IOT_PRINTF_FUNC("IoTGpioInit failed :%#x \r\n", ret);
        return ret;
    }

    /* 根据引脚号确定引脚IIC功能配置值及IIC端口号 */
    u8 sdaFunc = 0;
    u32 i2cPort = 0;
    ret = Gpio2IIC(pinSDA, &sdaFunc, &i2cPort);
    if(ret != KP_ERR_SUCCESS){ 
        IOT_PRINTF_FUNC("Gpio2IIC failed :%#x \r\n", ret);
        return ret;
    }

    /* 设置复用为IIC功能 */
    ret = IoTGpioSetFunc(pinSDA, sdaFunc);
    if(ret != KP_ERR_SUCCESS){ 
        IOT_PRINTF_FUNC("IoTGpioSetFunc failed :%#x \r\n", ret);
        return ret;
    }

    /* 根据引脚号确定引脚IIC功能配置值及IIC端口号 */
    u8 sclFunc = 0;
    ret = Gpio2IIC(pinSCL, &sclFunc, &i2cPort);
    if(ret != KP_ERR_SUCCESS){ 
        IOT_PRINTF_FUNC("Gpio2IIC failed :%#x \r\n", ret);
        return ret;
    }
    
	/* 设置复用为IIC功能 */
	ret = IoTGpioSetFunc(pinSCL, sclFunc);
	if(ret != 0){ 
        IOT_PRINTF_FUNC("IoTGpioSetFunc failed :%#x \r\n", ret);
		return ret;
    }

    /* IIC初始化 */
	ret = IoTI2cInit(i2cPort, baud);
    if (ret != KP_ERR_SUCCESS) {
        IOT_PRINTF_FUNC("IoTI2cInit failed :%#x \r\n", ret); 
        return ret;
    }

    /* IIC设置波特率 */
	ret = IoTI2cSetBaudrate(i2cPort, baud);
    if (ret != KP_ERR_SUCCESS) {
        IOT_PRINTF_FUNC("IoTI2cSetBaudrate failed :%#x \r\n", ret); 
        return ret;
    }
    
    return KP_ERR_SUCCESS;
}

