#ifndef  __IOT_GPIO_PWM_H__
#define __IOT_GPIO_PWM_H__

#include "iot_base_type.h"


/* GPIO初始化复用为PWM功能 */
u32 GpioInitPwm(u32 pin);

/* PWM控制 */
u32 GpioCtrlPwm(u32 pwmPort, u32 state, u16 duty, u32 freq);



#endif

