/*
 * Copyright (c) 2020 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "nativeapi_communication_kit.h"
#include "nativeapi_communication_kit_impl.h"

#include <securec.h>
#include "ability_env.h"
#include "js_async_work.h"
#include "nativeapi_common.h"
#include "nativeapi_config.h"


namespace OHOS {
namespace ACELite {
namespace {
char g_kvFullPath[FILE_NAME_MAX_LEN + 1] = {0};

bool IsValidKey(const char* key)
{
    if (key == nullptr) {
        return false;
    }
    size_t keyLen = strnlen(key, KEY_MAX_LEN + 1);
    if ((keyLen == 0) || (keyLen > KEY_MAX_LEN)) {
        return false;
    }
    if (strpbrk(key, "/\\\"*+,:;<=>\?[]|\x7F")) {
        return false;
    }
    return true;
}

JSIValue ExecuteAsyncWork(const JSIValue thisVal, const JSIValue* args,
    uint8_t argsNum, AsyncWorkHandler ExecuteFunc, bool flag)
{
    JSIValue undefValue = JSI::CreateUndefined();
    if (!NativeapiCommon::IsValidJSIValue(args, argsNum)) {
        return undefValue;
    }
    FuncParams* params = new FuncParams();
    if (params == nullptr) {
        return undefValue;
    }
    params->thisVal = JSI::AcquireValue(thisVal);
    params->args = JSI::AcquireValue(args[0]);
    params->flag = flag;
    JsAsyncWork::DispatchAsyncWork(ExecuteFunc, reinterpret_cast<void *>(params));
    return undefValue;
}

void ExecuteGet(void* data)
{
    FuncParams* params = reinterpret_cast<FuncParams *>(data);
    if (params == nullptr) {
        return;
    }

    JSIValue args = params->args;
    JSIValue thisVal = params->thisVal;
    char* key = JSI::GetStringProperty(args, KV_KEY);
    const char* dataPath = GetDataPath();
    JSIValue result = JSI::CreateUndefined();
    int ret = ERROR_CODE_GENERAL;
    char* value = reinterpret_cast<char *>(malloc(VALUE_MAX_LEN + 1));
    if (value == nullptr) {
        NativeapiCommon::FailCallBack(thisVal, args, ret);
        goto EXIT;
    }

    ret = GetCommunicationKitValue(key, value);

    if (ret == ERROR_FR_NO_FILE) {
        // GetDefault(thisVal, args);
        goto EXIT;
    }
    if (ret != NATIVE_SUCCESS) {
        NativeapiCommon::FailCallBack(thisVal, args, ret);
        goto EXIT;
    }
    result = JSI::CreateString(value);
    NativeapiCommon::SuccessCallBack(thisVal, args, result);
EXIT:
    free(value);
    JSI::ReleaseString(key);
    JSI::ReleaseValueList(args, thisVal, result, ARGS_END);
    delete params;
}

void ExecuteSet(void* data)
{
    FuncParams* params = reinterpret_cast<FuncParams *>(data);
    if (params == nullptr) {
        return;
    }

    JSIValue args = params->args;
    JSIValue thisVal = params->thisVal;
    int ret = ERROR_CODE_GENERAL;

    char* key = JSI::GetStringProperty(args, KV_KEY);
    char* value = JSI::GetStringProperty(args, KV_VALUE);

    LOG_I("JsSet:ExecuteSet::key=%s,value=%s\r\n",key,value);

    if ((key == nullptr) || !strlen(key)) {
        NativeapiCommon::FailCallBack(thisVal, args, ERROR_CODE_PARAM);
        goto EXIT;
    }
    if ((value == nullptr) || !strlen(value)) {
        NativeapiCommon::SuccessCallBack(thisVal, args, JSI::CreateUndefined());
        goto EXIT;
    }
    ret = SetCommunicationKitValue(key, value);
    if (ret != NATIVE_SUCCESS) {
        NativeapiCommon::FailCallBack(thisVal, args, ret);
        goto EXIT;
    }
    NativeapiCommon::SuccessCallBack(thisVal, args, JSI::CreateUndefined());
    
EXIT:
    JSI::ReleaseString(key);
    JSI::ReleaseString(value);
    JSI::ReleaseValueList(args, thisVal, ARGS_END);
    delete params;
}

}

void InitNativeApiCommunicationKit(JSIValue exports)
{
    JSI::SetModuleAPI(exports, "get", NativeapiCommunicationKit::Get);
    JSI::SetModuleAPI(exports, "set", NativeapiCommunicationKit::Set);
}

JSIValue NativeapiCommunicationKit::Get(const JSIValue thisVal, const JSIValue* args, uint8_t argsNum)
{
    return ExecuteAsyncWork(thisVal, args, argsNum, ExecuteGet, false);
}

JSIValue NativeapiCommunicationKit::Set(const JSIValue thisVal, const JSIValue* args, uint8_t argsNum)
{
    return ExecuteAsyncWork(thisVal, args, argsNum, ExecuteSet, false);
}

} // ACELite
} // OHOS
