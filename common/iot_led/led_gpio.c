#include "wifiiot_gpio.h"
#include "iot_gpio.h"
#include "iot_gpio_ex.h"
#include "led_gpio.h"

static unsigned int spin = 0;


/* 初始化LED */
u32 LedGpioInit(u32 pin)
{
	/* LED灯根据输入参数GPIO引脚初始化 */
    u32 ret = IoTGpioInit(pin);
	if(ret != KP_ERR_SUCCESS){ 
        printf("IoTGpioInit failed :%#x \r\n",ret);
		return ret;
    }

    /* 保存引脚号 */
    spin = pin;

	/* 根据引脚号确定引脚GPIO功能配置值 */
    u8 pinFunc = 0;
    switch (pin)
    {
        case WIFI_IOT_GPIO_IDX_0:
            pinFunc = IOT_GPIO_FUNC_GPIO_0_GPIO;
            break;
        case WIFI_IOT_GPIO_IDX_1:
            pinFunc = IOT_GPIO_FUNC_GPIO_1_GPIO;
            break;
        case WIFI_IOT_GPIO_IDX_2:
            pinFunc = IOT_GPIO_FUNC_GPIO_2_GPIO;
            break;
        case WIFI_IOT_GPIO_IDX_3:
            pinFunc = IOT_GPIO_FUNC_GPIO_3_GPIO;
            break;
        case WIFI_IOT_GPIO_IDX_4:
            pinFunc = IOT_GPIO_FUNC_GPIO_4_GPIO;
            break;
        case WIFI_IOT_GPIO_IDX_5:
            pinFunc = IOT_GPIO_FUNC_GPIO_5_GPIO;
            break;
        case WIFI_IOT_GPIO_IDX_6:
            pinFunc = IOT_GPIO_FUNC_GPIO_6_GPIO;
            break;
        case WIFI_IOT_GPIO_IDX_7:
            pinFunc = IOT_GPIO_FUNC_GPIO_7_GPIO;
            break;
        case WIFI_IOT_GPIO_IDX_8:
            pinFunc = IOT_GPIO_FUNC_GPIO_8_GPIO;
            break;
        case WIFI_IOT_GPIO_IDX_9:
            pinFunc = IOT_GPIO_FUNC_GPIO_9_GPIO;
            break;
        case WIFI_IOT_GPIO_IDX_10:
            pinFunc = IOT_GPIO_FUNC_GPIO_10_GPIO;
            break;
        case WIFI_IOT_GPIO_IDX_11:
            pinFunc = IOT_GPIO_FUNC_GPIO_11_GPIO;
            break;
        case WIFI_IOT_GPIO_IDX_12:
            pinFunc = IOT_GPIO_FUNC_GPIO_12_GPIO;
            break;
        case WIFI_IOT_GPIO_IDX_13:
            pinFunc = IOT_GPIO_FUNC_GPIO_13_GPIO;
            break;
        case WIFI_IOT_GPIO_IDX_14:
            pinFunc = IOT_GPIO_FUNC_GPIO_14_GPIO;
            break;
        default:
            return KP_ERR_FAILURE;
    }
    
    /* 设置复用为GPIO功能 */
	ret = IoTGpioSetFunc(pin, pinFunc);
	if(ret != KP_ERR_SUCCESS){ 
        printf("IoTGpioSetFunc failed :%#x \r\n", ret);
		return ret;
    }

	/* 设置成输出有效，通过输出高电平点亮LED灯 */
	ret = IoTGpioSetDir(pin, IOT_GPIO_DIR_OUT);
	if(ret != KP_ERR_SUCCESS){ 
        printf("IoTGpioSetDir failed :%#x \r\n", ret);
		return ret;
    }

    return KP_ERR_SUCCESS;
}

/* 点亮或熄灭LED灯 */
void LedGpioCtrl(u32 state)
{
	switch (state){
		case LED_OFF:
		{
		    /* LED灯熄灭 */
			IoTGpioSetOutputVal(spin, GPIO_OUT_LOW);
			printf("----- safeguard alarm led turn off -----\r\n");
		    break;
		}
		case LED_ON:
    	{
    	    /* LED灯点亮 */
			IoTGpioSetOutputVal(spin, GPIO_OUT_HIGH);
			printf("----- safeguard alarm led turn on -----\r\n");
		    break;
		}
		case LED_SPARK:
		{
		    /* LED灯闪烁 */
		    IoTGpioSetOutputVal(spin, GPIO_OUT_HIGH);
			osDelay(LED_SPARK_INTERVAL_TIME);
			IoTGpioSetOutputVal(spin, GPIO_OUT_LOW);
			osDelay(LED_SPARK_INTERVAL_TIME);
		    IoTGpioSetOutputVal(spin, GPIO_OUT_HIGH);
			osDelay(LED_SPARK_INTERVAL_TIME);
			IoTGpioSetOutputVal(spin, GPIO_OUT_LOW);
			break;
		}
		default:
		    break;
    }
}

