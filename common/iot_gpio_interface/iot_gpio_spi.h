#ifndef __SPI_SCREEN_H__
#define __SPI_SCREEN_H__

#include <hi_spi.h>
#include "iot_spi.h"
#include "iot_base_type.h"


#define test_spi_printf(fmt...)     \
    do {                            \
        printf("[SPI TEST]" fmt); \
        printf("\n");     \
    } while (0)

typedef struct {
    hi_spi_cfg_basic_info cfg_info;
    hi_spi_idx spi_id;
    hi_u32 loop;
    hi_u32 length;
    hi_bool irq;
    hi_bool slave;
    hi_bool lb;
    hi_bool dma_en;
} spi_para;


/* GPIO初始化复用为SPI功能，pinRES为SPI复位引脚，pinSPI为SPI控制引脚，可以是SPI四线中的任意一根 */
u32 GpioInitSpi(u32 pinRES, u32 pinRXD, u32 pinCS);


#endif
