#include <stdio.h>
#include <unistd.h>
#include "ohos_init.h"
#include "cmsis_os2.h"
#include "iot_gpio.h"
#include "kp_base_type.h"
#include "test.h"
#include "oled_module.h"
#include "oled_GME12864.h"

#define TASK_DELAY 200


/* 温湿度传感器处理任务，周期查询温度和湿度数据 */
void EnvOledTask(void)
{    
    while (1) {
        //hi_watchdog_feed();
        /* 在OLED上显示 */ 
        OLED_Clear(0);
        // TEST_Menu2();
        while(1){
       // TEST_Name();
       TEST_Chinese();
        osDelay(TASK_DELAY);
        }
    }

    return;
}

void EnvModuleSample(void)
{
    /* 各模块初始化 */
    printf(">> OLED_Init_GPIO \r\n");
    /* 初始化OLED  */
	OLED_Init_GPIO();
    
    OLED_Init();               //初始化OLED
    
    // osDelay(100);

	/* 清屏（全黑） */
    OLED_Clear(0);
 

    /* 设置线程属性 */
    osThreadAttr_t attr;
    attr.name = "EnvOledTask";
    attr.attr_bits = 0U;
    attr.cb_mem = NULL;
    attr.cb_size = 0U;
    attr.stack_mem = NULL;
    attr.stack_size = 1024 * 4;
    attr.priority = 25;
    
        EnvOledTask();


    // /* 创建温湿度处理线程 */
    // osThreadId_t threadIDSht = osThreadNew((osThreadFunc_t)EnvOledTask, NULL, &attr);
    // if (threadIDSht == NULL)
    // {
    //     printf("Falied to create EnvShtTask!\r\n");
    // } else {
    //     printf("success to create EnvShtTask!\r\n");
    // }
}

APP_FEATURE_INIT(EnvModuleSample);

