#ifndef __EBOX_GAS_LED_H__
#define __EBOX_GAS_LED_H__


//初始化可燃气体检测模块的LED报警灯
void LedInit(void);

//控制可燃气体检测模块的LED报警灯，0:熄灭；1:点亮; 2:闪烁
void LedCtrl(u32 state);


#endif
