#include <stdio.h>  // for printf
#include <string.h> // for memory operation
#include <stdlib.h>
#include <unistd.h>  // for sleep
#include "ohos_init.h"
#include "cmsis_os2.h"
#include "lwip/netifapi.h"
#include "lwip/api_shell.h"
#include "lwip/sockets.h"
#include "wifi_sta.h"
#include <hi_early_debug.h>
#include "hi_task.h"

#define CONFIG_WIFI_SSID "nj" // 修改为自己的WiFi 热点账号
#define CONFIG_WIFI_PWD "12345678" // 修改为自己的WiFi 热点密码

#define UDP_CONNECT_IPADDR  "192.168.212.145"
#define UDP_CONNECT_PORT 5555


char recvbuf[512] = {0};
const char *qihangsendmsg = "Hello! This is UDP Client!";

/* 按照注释要求补全函数功能代码 */
/*任务执行函数，启动UDP客户端*/
static int UdpClientTask() {
    struct sockaddr_in send_addr = {0};
    int ret = 0;

    /* 连接wifi热点 */
    WifiConnect(CONFIG_WIFI_SSID,CONFIG_WIFI_PWD);
    sleep(2);

    /* 创建socket */
    int socketfd = socket(AF_INET,SOCK_DGRAM,0);
    if(socketfd == -1)
    {
        printf("UdpClientTask:  create socket failed!\r\n");
        return -1;
    }else{
        printf("UdpClientTask:  create socket success!\r\n");
    }

    /* 1、初始化UDP服务器端地址 
    2、发送数据给UDP服务器
    3、从UDP服务器接收数据并打印
    */
    send_addr.sin_family = AF_INET;
    send_addr.sin_addr.s_addr = inet_addr(UDP_CONNECT_IPADDR);
    send_addr.sin_port = htons(UDP_CONNECT_PORT);

    int sin_size = sizeof(struct sockaddr_in);
    while(1)
    {
        memset(recvbuf,0,sizeof(recvbuf));

        sleep(2);
        if((ret = sendto(socketfd,qihangsendmsg,strlen(qihangsendmsg),0,(struct sockaddr*)&send_addr,sizeof(send_addr))) == -1)
        {
            printf("UdpClientTaks:send failed,error is %d\r\n",errno);
            break;
        }else{
            printf("send :%s\r\n",qihangsendmsg);
        }
        sleep(10);
        if((ret = recvfrom(socketfd,recvbuf,sizeof(recvbuf),0,(struct sockaddr *)&send_addr,(socklen_t *)&sin_size)) == -1)
        {
            printf("UdpClienTask:recv failed,error is %d\r\n",errno);
        }else{
            printf("recv :%s\r\n",recvbuf);
        }

    }

    closesocket(socketfd);
    return 0;
}

void UdpClientSample()
{
    osThreadAttr_t attr;
    attr.name = "UdpClientTask";
    attr.attr_bits = 0U;
    attr.cb_mem = NULL;
    attr.cb_size = 0U;
    attr.stack_mem = NULL;
    attr.stack_size = 4096;
    attr.priority = 20;
    if (osThreadNew((osThreadFunc_t)UdpClientTask, NULL, &attr) == NULL) {
        printf("Failed to create UdpClientTask!\n");
    }
}

APP_FEATURE_INIT(UdpClientSample);