#ifndef __SSD1306_DRV_H__
#define __SSD1306_DRV_H__

#include "iot_gpio.h"
#include "wifiiot_gpio.h"
#include "iot_spi.h"
#include "iot_base_type.h"

//--------------SSD1306参数定义---------------------
#define PAGE_SIZE    8
#define XLevelL      0x02 //1.3寸XLevelL为0x02 0.9寸XLevelL为0x00
#define XLevelH      0x10
#define YLevel       0xB0
#define	Brightness   0xFF

#ifndef SSD1306_WIDTH
#define SSD1306_WIDTH        128
#endif

#ifndef SSD1306_HEIGHT
#define SSD1306_HEIGHT       64
#endif

//数组每个bit存储OLED每个像素点的颜色值(1-亮(白色),0-灭(黑色))
//SSD1306共64*128点阵，每个数组元素为8位，表示1列8个像素点，一共128列，
//因此数组的大小为64/8*128=1024
#ifndef SSD1306_BUFFER_SIZE
#define SSD1306_BUFFER_SIZE   SSD1306_WIDTH * SSD1306_HEIGHT / 8
#endif

// Enumeration for screen colors
typedef enum {
    Black = 0x00, // Black color, no pixel
    White = 0x01  // Pixel is set. Color depends on OLED
} SSD1306_COLOR;

// Struct to store transformations
typedef struct {
    u16     CurrentX;
    u16     CurrentY;
    u8      Inverted;
    u8 Initialized;
    u8 DisplayOn;
} SSD1306_t;

typedef struct {
    u8  x;
    u8  y;
} SSD1306_VERTEX;



//-------------SSD1306写命令和数据定义-------------------
#define SSD1306_CMD     0  //D/CX为低电平时写命令
#define SSD1306_DATA    1  //D/CX为高电平时写数据

//-----------------SSD1306端口操作定义---------------- 
/* SPI方式控制SSD1306 */
#if SSD1306_USE_SPI
#define SSD1306_CS_Clr(...) IoTGpioSetOutputVal(WIFI_IOT_GPIO_IDX_12, IOT_GPIO_VALUE0, ##__VA_ARGS__)
#define SSD1306_CS_Set(...) IoTGpioSetOutputVal(WIFI_IOT_GPIO_IDX_12, IOT_GPIO_VALUE1, ##__VA_ARGS__)

#define SSD1306_DC_Clr(...) IoTGpioSetOutputVal(WIFI_IOT_GPIO_IDX_11, IOT_GPIO_VALUE0, ##__VA_ARGS__)
#define SSD1306_DC_Set(...) IoTGpioSetOutputVal(WIFI_IOT_GPIO_IDX_11, IOT_GPIO_VALUE1, ##__VA_ARGS__)

#define SSD1306_RST_Clr(...)  IoTGpioSetOutputVal(WIFI_IOT_GPIO_IDX_8, IOT_GPIO_VALUE0, ##__VA_ARGS__)
#define SSD1306_RST_Set(...)  IoTGpioSetOutputVal(WIFI_IOT_GPIO_IDX_8, IOT_GPIO_VALUE1, ##__VA_ARGS__)
#endif
#if 0
#define OLED_CS_Clr() hi_gpio_set_ouput_val(HI_GPIO_IDX_12,HI_GPIO_VALUE0);
#define OLED_CS_Set() hi_gpio_set_ouput_val(HI_GPIO_IDX_12,HI_GPIO_VALUE1);

#define OLED_DC_Clr()  hi_gpio_set_ouput_val(HI_GPIO_IDX_11,HI_GPIO_VALUE0);
#define OLED_DC_Set()  hi_gpio_set_ouput_val(HI_GPIO_IDX_11,HI_GPIO_VALUE1);

#define OLED_RST_Clr()  hi_gpio_set_ouput_val(HI_GPIO_IDX_8,HI_GPIO_VALUE0);
#define OLED_RST_Set()  hi_gpio_set_ouput_val(HI_GPIO_IDX_8,HI_GPIO_VALUE1);

#define SPI_WriteByte(SPI2,dat) SpiWriteData(0,dat);
#endif

//SSD1306初始化
void SSD1306InitGPIO(void);
void SSD1306InitIO(u8 spiId);
void SSD1306Init(void);


//SSD1306控制用函数
u32 SSD1306WriteBuf(u32 spiId, u8 *data, u32 byteLen);
u32 SSD1306WriteCmd(u32 spiId, u8 cmd);
#define SSD1306WRCmd(...) SSD1306WriteCmd(IOT_SPI_ID_0, ##__VA_ARGS__)
u32 SSD1306WriteData(u32 spiId, u8 *data, u32 byteLen);
#define SSD1306WRData(...) SSD1306WriteData(IOT_SPI_ID_0, ##__VA_ARGS__)
void SSD1306SetPos(unsigned char x, unsigned char y);
void SSD1306DisplayOn(void);
void SSD1306DisplayOff(void);
void SSD1306SetPixel(unsigned char x, unsigned char y,unsigned char color);
void SSD1306Display(void);
void SSD1306Clear(unsigned dat);
void SSD1306Reset(void);


#endif

