#ifndef __KP_BASE_TYPE_H__
#define __KP_BASE_TYPE_H__

#include <stdio.h>

/* 基本数据类型 */
typedef unsigned char           u8;
typedef unsigned short          u16;
typedef unsigned int            u32;
typedef unsigned long long      u64;
typedef unsigned long           ulong;
typedef char                    s8;
typedef short                   s16;
typedef int                     s32;
typedef long long               s64;
typedef long                    slong;

#undef ERROR
#define ERROR (-1)

#define KP_ERR_SUCCESS  0
#define KP_ERR_FAILURE  (-1)

#ifndef NULL
#define NULL ((void *)0)
#endif

#define KP_U8_MAX            0xFF
#define KP_U16_MAX            0xFFFF
#define KP_U32_MAX            0xFFFFFFFF
#define KP_U64_MAX            0xFFFFFFFFFFFFFFFFUL

#include <unistd.h>
#define msleep(x)               usleep((x)*1000)

typedef enum {
    GPIO_OUT_LOW = 0,      /* 低电平 */
    GPIO_OUT_HIGH = 1          /* 高电平 */
} gpio_out_value;

typedef enum {
    PWM_PORT_PWM0 = 0, /* PWM0端口 */
    PWM_PORT_PWM1 = 1, /* PWM1端口 */
    PWM_PORT_PWM2 = 2, /* PWM2端口 */
    PWM_PORT_PWM3 = 3, /* PWM3端口 */
    PWM_PORT_PWM4 = 4, /* PWM4端口 */
    PWM_PORT_PWM5 = 5, /* PWM5端口 */
    PWM_PORT_MAX   /* 最大值，不可使用 */
} pwm_port;

typedef enum {
    LED_OFF = 0,
    LED_ON = 1,
    LED_SPARK = 2
} led_state;
#define LED_SPARK_INTERVAL_TIME 30

#define KP_DEBUG(fmt, args...)  do {                \
		printf("[KP][%s][%d]", __FILE__, __LINE__); \
	    printf(fmt, ##args);                        \
	    printf("\r\n");                             \
    } while (0)


#endif
