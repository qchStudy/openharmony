#ifndef __IOT_GPIO_FUNC_H__
#define __IOT_GPIO_FUNC_H__

#include "iot_base_type.h"


/* 通过GPIO引脚号获取GPIO功能编号 */
u32 Gpio2Gpio(u32 gpio, u8 *func);

/* 通过GPIO引脚号获取GPIO功能编号 */
u32 Gpio2Uart(u32 gpio, u8 *func, u32 *uartId);

/* 通过GPIO引脚号获取SPI功能编号 */
u32 Gpio2Spi(u32 gpio, u8 *func, u32 *spiId);

/* 通过GPIO引脚号获取PWM功能编号 */
u32 Gpio2Pwm(u32 gpio, u8 *func, u32 *pwmId);

/* 通过GPIO引脚号获取IIC功能编号 */
u32 Gpio2IIC(u32 gpio, u8 *func, u32 *i2cId);



#endif

