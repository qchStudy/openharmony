#include <stdio.h>
#include <unistd.h>
#include "lwip/api_shell.h"
#include "lwip/ip4_addr.h"
#include "lwip/netif.h"
#include "lwip/netifapi.h"
#include "cmsis_os2.h"
#include "ohos_init.h"
#include "wifi_device.h"
#include "wifi_error_code.h"

#define DEF_TIMEOUT 15
#define ONE_SECOND 1
static int g_staScanSuccess = 0;
static int g_connectSuccess = 0;
static int g_ssid_count = 0;
static struct netif* g_lwip_netif = NULL;
static WifiEvent g_wifiEventHandler = {0};

#define SELECT_WLAN_PORT "wlan0"
#define SELECT_WIFI_SECURITYTYPE WIFI_SEC_TYPE_PSK

static void OnWifiScanStateChangedHandler(int state, int size)
{
    (void)state;
    if (size > 0) {
        g_ssid_count = size;
        g_staScanSuccess = 1;
    }
    printf("callback function for wifi scan:%d, %d\r\n", state, size);
    return;
}

static void OnWifiConnectionChangedHandler(int state, WifiLinkedInfo* info)
{
    (void)info;

    if (state > 0) {
        g_connectSuccess = 1;
        printf("callback function for wifi connect\r\n");
    } else {
        printf("connect error,please check password\r\n");
    }
}

static void WaitSacnResult(void)
{
    int scanTimeout = DEF_TIMEOUT;
    while (scanTimeout > 0) {
        sleep(ONE_SECOND);
        scanTimeout--;
        if (g_staScanSuccess == 1) {
            printf("WaitSacnResult:wait success[%d]s\n", (DEF_TIMEOUT - scanTimeout));
            break;
        }
    }
    if (scanTimeout <= 0) {
        printf("WaitSacnResult:timeout!\n");
    }
}

static int WaitConnectResult(void)
{
    int ConnectTimeout = DEF_TIMEOUT;
    while (ConnectTimeout > 0) {
        sleep(ONE_SECOND);
        ConnectTimeout--;
        if (g_connectSuccess == 1) {
            printf("WaitConnectResult:wait success[%d]s\n", (DEF_TIMEOUT - ConnectTimeout));
            break;
        }
    }
    if (ConnectTimeout <= 0) {
        printf("WaitConnectResult:timeout!\n");
        return 0;
    }

    return 1;
}

/* 按照注释要求补全函数功能代码 */
bool WifiConnect(const char *ssid, const char *pwd)
{
    WifiScanInfo* info = NULL;
    unsigned int size = WIFI_SCAN_HOTSPOT_LIMIT;
    WifiDeviceConfig select_ap_config = {0};
    WifiErrorCode error = WIFI_SUCCESS;

    //延时2S便于查看日志
    osDelay(200);
	
	/* 注册wifi事件的回调函数，如果失败打印错误码 */
    g_wifiEventHandler.OnWifiScanStateChanged = OnWifiScanStateChangedHandler;
    g_wifiEventHandler.OnWifiConnectionChanged = OnWifiConnectionChangedHandler;
    // g_wifiEventHandler.OnHotspotStaJoin = OnHotspotStaJoinHandler;
    // g_wifiEventHandler.OnHotspotStaLeave = OnHotspotStaLeaveHandler;
    // g_wifiEventHandler.OnHotspotStateChanged = OnHotspotStateChangedHandler;
    error = RegisterWifiEvent(&g_wifiEventHandler);
    if(error != WIFI_SUCCESS)
    {
        printf("RegisterWifiEvent failed, error = %d.\r\n",error);
    }
    printf("RegisterWifiEvent succeed!\r\n");
	
	/* 启用Wi-Fi功能，如果失败打印错误码 */
	if((error = EnableWifi()) != WIFI_SUCCESS)
    {
        printf("EnableWifi failed, error = %d\r\n",error);
    }
	
    /* 判断WIFI是否成功激活，如果失败打印错误码 */
    if(IsWifiActive() == 0)
    {
        printf("wifi station is not actived.\r\n");
        return 0;
    }

    //分配空间，保存WiFi信息
    info = malloc(sizeof(WifiScanInfo) * WIFI_SCAN_HOTSPOT_LIMIT);
    if (info == NULL) {
        return 0;
    }

    //循环查找WiFi列表直至成功
    do {
        //重置标志位
        g_ssid_count = 0;
        g_staScanSuccess = 0;

        /* 开始扫描热点 */
        Scan();

        /* 等待扫描结果 */
        WaitSacnResult();

        /* 获取扫描到的热点列表 */
        error = GetScanInfoList(info,&size);
    } while (g_staScanSuccess != 1);

    //打印WiFi热点列表
    for (uint8_t i = 0; i < g_ssid_count; i++) {
        printf("no:%03d, ssid:%-30s, rssi:%5d\r\n", i + 1, info[i].ssid, info[i].rssi / 100);
    }

    //连接指定的WiFi热点
    for (int i = 0; i < g_ssid_count; i++) {
        /* 1、从扫描到的热点列表中寻找是否有目标热点
        2、如果找到，则添加热点配置信息，连接热点并判断是否成功
        */
        if(strcmp(ssid,info[i].ssid) == 0)
        {
            int result;
            strcpy(select_ap_config.ssid,info[i].ssid);
            strcpy(select_ap_config.preSharedKey,pwd);
            select_ap_config.securityType = SELECT_WIFI_SECURITYTYPE;
            if(AddDeviceConfig(&select_ap_config,&result) == WIFI_SUCCESS)
            {
                if(ConnectTo(result) == WIFI_SUCCESS && WaitConnectResult() == 1)
                {
                    printf("WIFI connect succeed!\r\n");
                    g_lwip_netif = netifapi_netif_find(SELECT_WLAN_PORT);
                    break;
                }
            }
        }

        if (i == g_ssid_count - 1) {
            printf("ERROR: No wifi as expected\r\n");
            while (1)
                osDelay(100);
        }
    }

    //启动DHCP
    if (g_lwip_netif) {
        dhcp_start(g_lwip_netif);
        printf("begin to dhcp\r\n");
    }

    //等待DHCP
    for (;;) {
        if (dhcp_is_bound(g_lwip_netif) == ERR_OK) {
            printf("<-- DHCP state:OK -->\r\n");

            //打印获取到的IP信息
            netifapi_netif_common(g_lwip_netif, dhcp_clients_info_show, NULL);
            break;
        }
        printf("<-- DHCP state:Inprogress -->\r\n");
        osDelay(100);
    }

    return 1;
}

