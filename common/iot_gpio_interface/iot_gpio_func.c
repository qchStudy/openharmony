#include "wifiiot_gpio.h"
#include "iot_gpio_ex.h"
#include "iot_uart.h"
#include "iot_spi.h"
#include "iot_pwm.h"
#include "iot_i2c.h"
#include "iot_gpio_func.h"


/* 通过GPIO引脚号获取GPIO功能编号 */
u32 Gpio2Gpio(u32 gpio, u8 *func)
{
    u32 ret = KP_ERR_SUCCESS;
    
    /* 根据引脚号确定引脚GPIO功能编号 */
    switch (gpio)
    {
        case WIFI_IOT_GPIO_IDX_0:
            func = IOT_GPIO_FUNC_GPIO_0_GPIO;
            break;
        case WIFI_IOT_GPIO_IDX_1:
            func = IOT_GPIO_FUNC_GPIO_1_GPIO;
            break;
        case WIFI_IOT_GPIO_IDX_2:
            func = IOT_GPIO_FUNC_GPIO_2_GPIO;
            break;
        case WIFI_IOT_GPIO_IDX_3:
            func = IOT_GPIO_FUNC_GPIO_3_GPIO;
            break;
        case WIFI_IOT_GPIO_IDX_4:
            func = IOT_GPIO_FUNC_GPIO_4_GPIO;
            break;
        case WIFI_IOT_GPIO_IDX_5:
            func = IOT_GPIO_FUNC_GPIO_5_GPIO;
            break;
        case WIFI_IOT_GPIO_IDX_6:
            func = IOT_GPIO_FUNC_GPIO_6_GPIO;
            break;
        case WIFI_IOT_GPIO_IDX_7:
            func = IOT_GPIO_FUNC_GPIO_7_GPIO;
            break;
        case WIFI_IOT_GPIO_IDX_8:
            func = IOT_GPIO_FUNC_GPIO_8_GPIO;
            break;
        case WIFI_IOT_GPIO_IDX_9:
            func = IOT_GPIO_FUNC_GPIO_9_GPIO;
            break;
        case WIFI_IOT_GPIO_IDX_10:
            func = IOT_GPIO_FUNC_GPIO_10_GPIO;
            break;
        case WIFI_IOT_GPIO_IDX_11:
            func = IOT_GPIO_FUNC_GPIO_11_GPIO;
            break;
        case WIFI_IOT_GPIO_IDX_12:
            func = IOT_GPIO_FUNC_GPIO_12_GPIO;
            break;
        case WIFI_IOT_GPIO_IDX_13:
            func = IOT_GPIO_FUNC_GPIO_13_GPIO;
            break;
        case WIFI_IOT_GPIO_IDX_14:
            func = IOT_GPIO_FUNC_GPIO_14_GPIO;
            break;
        default:
            IOT_PRINTF_FUNC("Invalid gpio :%d \r\n", gpio);
            return KP_ERR_FAILURE;
    }
    
    return KP_ERR_SUCCESS;
}

/* 通过GPIO引脚号获取GPIO功能编号 */
u32 Gpio2Uart(u32 gpio, u8 *func, u32 *uartId)
{
    u32 ret = KP_ERR_SUCCESS;
    
    /* 根据引脚号确定引脚GPIO功能编号 */
    switch (gpio)
    {
        case WIFI_IOT_GPIO_IDX_0:
            func = IOT_GPIO_FUNC_GPIO_0_UART1_TXD;
            uartId = IOT_UART_IDX_1;
            break;
        case WIFI_IOT_GPIO_IDX_1:
            func = IOT_GPIO_FUNC_GPIO_1_UART1_RXD;
            uartId = IOT_UART_IDX_1;
            break;
        case WIFI_IOT_GPIO_IDX_3:
            func = IOT_GPIO_FUNC_GPIO_3_UART0_TXD;
            uartId = IOT_UART_IDX_0;
            break;
        case WIFI_IOT_GPIO_IDX_4:
            func = IOT_GPIO_FUNC_GPIO_4_UART0_RXD;
            uartId = IOT_UART_IDX_0;
            break;
        case WIFI_IOT_GPIO_IDX_5:
            func = IOT_GPIO_FUNC_GPIO_5_UART1_RXD;
            uartId = IOT_UART_IDX_1;
            break;
        case WIFI_IOT_GPIO_IDX_6:
            func = IOT_GPIO_FUNC_GPIO_6_UART1_TXD;
            uartId = IOT_UART_IDX_1;
            break;
        case WIFI_IOT_GPIO_IDX_11:
            func = IOT_GPIO_FUNC_GPIO_11_UART2_TXD;
            uartId = IOT_UART_IDX_2;
            break;
        case WIFI_IOT_GPIO_IDX_12:
            func = IOT_GPIO_FUNC_GPIO_12_UART2_RXD;
            uartId = IOT_UART_IDX_2;
            break;
        case WIFI_IOT_GPIO_IDX_13:
            func = IOT_GPIO_FUNC_GPIO_13_UART0_TXD;
            uartId = IOT_UART_IDX_0;
            break;
        case WIFI_IOT_GPIO_IDX_14:
            func = IOT_GPIO_FUNC_GPIO_14_UART0_RXD;
            uartId = IOT_UART_IDX_0;
            break;
        default:
            IOT_PRINTF_FUNC("Invalid gpio :%d \r\n", gpio);
            return KP_ERR_FAILURE;
    }
    
    return KP_ERR_SUCCESS;
}

/* 通过GPIO引脚号获取SPI功能编号 */
u32 Gpio2Spi(u32 gpio, u8 *func, u32 *spiId)
{
    u32 ret = KP_ERR_SUCCESS;
    
    /* 根据引脚号确定引脚GPIO功能编号 */
    switch (gpio)
    {
        case WIFI_IOT_GPIO_IDX_0:
            func = IOT_GPIO_FUNC_GPIO_0_SPI1_CK;
            spiId = IOT_SPI_ID_1;
            break;
        case WIFI_IOT_GPIO_IDX_1:
            func = IOT_GPIO_FUNC_GPIO_1_SPI1_RXD;
            spiId = IOT_SPI_ID_1;
            break;
        case WIFI_IOT_GPIO_IDX_2:
            func = IOT_GPIO_FUNC_GPIO_2_SPI1_TXD;
            spiId = IOT_SPI_ID_1;
            break;
        case WIFI_IOT_GPIO_IDX_3:
            func = IOT_GPIO_FUNC_GPIO_3_SPI1_CSN;
            spiId = IOT_SPI_ID_1;
            break;
        case WIFI_IOT_GPIO_IDX_5:
            func = IOT_GPIO_FUNC_GPIO_5_SPI0_CSN;
            spiId = IOT_SPI_ID_0;
            break;
        case WIFI_IOT_GPIO_IDX_6:
            func = IOT_GPIO_FUNC_GPIO_6_SPI0_CK;
            spiId = IOT_SPI_ID_0;
            break;
        case WIFI_IOT_GPIO_IDX_7:
            func = IOT_GPIO_FUNC_GPIO_7_SPI0_RXD;
            spiId = IOT_SPI_ID_0;
            break;
        case WIFI_IOT_GPIO_IDX_8:
            func = IOT_GPIO_FUNC_GPIO_8_SPI0_TXD;
            spiId = IOT_SPI_ID_0;
            break;
        case WIFI_IOT_GPIO_IDX_9:
            func = IOT_GPIO_FUNC_GPIO_9_SPI0_TXD;
            spiId = IOT_SPI_ID_0;
            break;
        case WIFI_IOT_GPIO_IDX_10:
            func = IOT_GPIO_FUNC_GPIO_10_SPI0_CK;
            spiId = IOT_SPI_ID_0;
            break;
        case WIFI_IOT_GPIO_IDX_11:
            func = IOT_GPIO_FUNC_GPIO_11_SPI0_RXD;
            spiId = IOT_SPI_ID_0;
            break;
        case WIFI_IOT_GPIO_IDX_12:
            func = IOT_GPIO_FUNC_GPIO_12_SPI0_CSN;
            spiId = IOT_SPI_ID_0;
            break;
        default:
            IOT_PRINTF_FUNC("Invalid gpio :%d \r\n", gpio);
            return KP_ERR_FAILURE;
    }
    
    return KP_ERR_SUCCESS;
}

/* 通过GPIO引脚号获取PWM功能编号 */
u32 Gpio2Pwm(u32 gpio, u8 *func, u32 *pwmId)
{
    u32 ret = KP_ERR_SUCCESS;
    
    /* 根据引脚号确定引脚GPIO功能编号 */
    switch (gpio)
    {
        case WIFI_IOT_GPIO_IDX_0:
            func = IOT_GPIO_FUNC_GPIO_0_PWM3_OUT;
            pwmId = PWM_PORT_PWM3;
            break;
        case WIFI_IOT_GPIO_IDX_1:
            func = IOT_GPIO_FUNC_GPIO_1_PWM4_OUT;
            pwmId = PWM_PORT_PWM4;
            break;
        case WIFI_IOT_GPIO_IDX_2:
            func = IOT_GPIO_FUNC_GPIO_2_PWM2_OUT;
            pwmId = PWM_PORT_PWM2;
            break;
        case WIFI_IOT_GPIO_IDX_3:
            func = IOT_GPIO_FUNC_GPIO_3_PWM5_OUT;
            pwmId = PWM_PORT_PWM5;
            break;
        case WIFI_IOT_GPIO_IDX_4:
            func = IOT_GPIO_FUNC_GPIO_4_PWM1_OUT;
            pwmId = PWM_PORT_PWM1;
            break;
        case WIFI_IOT_GPIO_IDX_5:
            func = IOT_GPIO_FUNC_GPIO_5_PWM2_OUT;
            pwmId = PWM_PORT_PWM2;
            break;
        case WIFI_IOT_GPIO_IDX_6:
            func = IOT_GPIO_FUNC_GPIO_6_PWM3_OUT;
            pwmId = PWM_PORT_PWM3;
            break;
        case WIFI_IOT_GPIO_IDX_7:
            func = IOT_GPIO_FUNC_GPIO_7_PWM0_OUT;
            pwmId = PWM_PORT_PWM0;
            break;
        case WIFI_IOT_GPIO_IDX_8:
            func = IOT_GPIO_FUNC_GPIO_8_PWM1_OUT;
            pwmId = PWM_PORT_PWM1;
            break;
        case WIFI_IOT_GPIO_IDX_9:
            func = IOT_GPIO_FUNC_GPIO_9_PWM0_OUT;
            pwmId = PWM_PORT_PWM0;
            break;
        case WIFI_IOT_GPIO_IDX_10:
            func = IOT_GPIO_FUNC_GPIO_10_PWM1_OUT;
            pwmId = PWM_PORT_PWM1;
            break;
        case WIFI_IOT_GPIO_IDX_11:
            func = IOT_GPIO_FUNC_GPIO_11_PWM2_OUT;
            pwmId = PWM_PORT_PWM2;
            break;
        case WIFI_IOT_GPIO_IDX_12:
            func = IOT_GPIO_FUNC_GPIO_12_PWM3_OUT;
            pwmId = PWM_PORT_PWM3;
            break;
        case WIFI_IOT_GPIO_IDX_13:
            func = IOT_GPIO_FUNC_GPIO_13_PWM4_OUT;
            pwmId = PWM_PORT_PWM4;
            break;
        case WIFI_IOT_GPIO_IDX_14:
            func = IOT_GPIO_FUNC_GPIO_14_PWM5_OUT;
            pwmId = PWM_PORT_PWM5;
            break;
        default:
            IOT_PRINTF_FUNC("Invalid gpio :%d \r\n", gpio);
            return KP_ERR_FAILURE;
    }
    
    return KP_ERR_SUCCESS;
}

/* 通过GPIO引脚号获取IIC功能编号 */
u32 Gpio2IIC(u32 gpio, u8 *func, u32 *i2cId)
{
    u32 ret = KP_ERR_SUCCESS;
    
    /* 根据引脚号确定引脚GPIO功能编号 */
    switch (gpio)
    {
        case WIFI_IOT_GPIO_IDX_0:
            func = IOT_GPIO_FUNC_GPIO_0_I2C1_SDA;
            i2cId = IOT_I2C_IDX_1;
            break;
        case WIFI_IOT_GPIO_IDX_1:
            func = IOT_GPIO_FUNC_GPIO_1_I2C1_SCL;
            i2cId = IOT_I2C_IDX_1;
            break;
        case WIFI_IOT_GPIO_IDX_3:
            func = IOT_GPIO_FUNC_GPIO_3_I2C1_SDA;
            i2cId = IOT_I2C_IDX_1;
            break;
        case WIFI_IOT_GPIO_IDX_4:
            func = IOT_GPIO_FUNC_GPIO_4_I2C1_SCL;
            i2cId = IOT_I2C_IDX_1;
            break;
        case WIFI_IOT_GPIO_IDX_9:
            func = IOT_GPIO_FUNC_GPIO_9_I2C0_SCL;
            i2cId = IOT_I2C_IDX_0;
            break;
        case WIFI_IOT_GPIO_IDX_10:
            func = IOT_GPIO_FUNC_GPIO_10_I2C0_SDA;
            i2cId = IOT_I2C_IDX_0;
            break;
        case WIFI_IOT_GPIO_IDX_13:
            func = IOT_GPIO_FUNC_GPIO_13_I2C0_SDA;
            i2cId = IOT_I2C_IDX_0;
            break;
        case WIFI_IOT_GPIO_IDX_14:
            func = IOT_GPIO_FUNC_GPIO_14_I2C0_SCL;
            i2cId = IOT_I2C_IDX_0;
            break;
        default:
            IOT_PRINTF_FUNC("Invalid gpio :%d \r\n", gpio);
            return KP_ERR_FAILURE;
    }
    
    return KP_ERR_SUCCESS;
}


