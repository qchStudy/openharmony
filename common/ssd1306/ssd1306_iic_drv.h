#ifndef __SSD1306_SPI_DRV_H__
#define __SSD1306_SPI_DRV_H__

#include "iot_gpio.h"
#include "wifiiot_gpio.h"
#include "iot_spi.h"
#include "iot_base_type.h"

//--------------SSD1306参数定义---------------------
#define PAGE_SIZE    8
#define XLevelL      0x02 //1.3寸XLevelL为0x02 0.9寸XLevelL为0x00
#define XLevelH      0x10
#define YLevel       0xB0
#define	Brightness   0xFF 
#define WIDTH        128
#define HEIGHT       64

//-------------SSD1306写命令和数据定义-------------------
#define SSD1306_CMD     0  //D/CX为低电平时写命令
#define SSD1306_DATA    1  //D/CX为高电平时写数据

//-----------------SSD1306端口操作定义---------------- 
#if 1
#define SSD1306_CS_Clr(...) IoTGpioSetOutputVal(WIFI_IOT_GPIO_IDX_12, IOT_GPIO_VALUE0, ##__VA_ARGS__)
#define SSD1306_CS_Set(...) IoTGpioSetOutputVal(WIFI_IOT_GPIO_IDX_12, IOT_GPIO_VALUE1, ##__VA_ARGS__)

#define SSD1306_DC_Clr(...) IoTGpioSetOutputVal(WIFI_IOT_GPIO_IDX_11, IOT_GPIO_VALUE0, ##__VA_ARGS__)
#define SSD1306_DC_Set(...) IoTGpioSetOutputVal(WIFI_IOT_GPIO_IDX_11, IOT_GPIO_VALUE1, ##__VA_ARGS__)

#define SSD1306_RST_Clr(...)  IoTGpioSetOutputVal(WIFI_IOT_GPIO_IDX_8, IOT_GPIO_VALUE0, ##__VA_ARGS__)
#define SSD1306_RST_Set(...)  IoTGpioSetOutputVal(WIFI_IOT_GPIO_IDX_8, IOT_GPIO_VALUE1, ##__VA_ARGS__)
#else
#define OLED_CS_Clr() hi_gpio_set_ouput_val(HI_GPIO_IDX_12,HI_GPIO_VALUE0);
#define OLED_CS_Set() hi_gpio_set_ouput_val(HI_GPIO_IDX_12,HI_GPIO_VALUE1);

#define OLED_DC_Clr()  hi_gpio_set_ouput_val(HI_GPIO_IDX_11,HI_GPIO_VALUE0);
#define OLED_DC_Set()  hi_gpio_set_ouput_val(HI_GPIO_IDX_11,HI_GPIO_VALUE1);

#define OLED_RST_Clr()  hi_gpio_set_ouput_val(HI_GPIO_IDX_8,HI_GPIO_VALUE0);
#define OLED_RST_Set()  hi_gpio_set_ouput_val(HI_GPIO_IDX_8,HI_GPIO_VALUE1);

#define SPI_WriteByte(SPI2,dat) SpiWriteData(0,dat);
#endif

/* IIC方式控制SSD1306 */
#if SSD1306_USE_IIC

//SSD1306初始化SPI
void SSD1306SpiInit(IotSpiIdx spiId);

//SSD1306控制用函数
u32 Ssd1306WriteData(u32 spiId, u8 data);
u32 Ssd1306WriteByte(u32 spiId, u8 dat, u32 dataType);
#define SSD1306WRByte(...) Ssd1306WriteByte(IOT_SPI_ID_0, ##__VA_ARGS__)
void SSD1306_WR_Byte(unsigned dat,unsigned cmd);
void SSD1306_Display_On(void);
void SSD1306_Display_Off(void);
void SSD1306_Set_Pos(unsigned char x, unsigned char y);
void SSD1306_Reset(void);
void SSD1306_Init_GPIO(void);
void SSD1306_Init(void);
void SSD1306_Set_Pixel(unsigned char x, unsigned char y,unsigned char color);
void SSD1306_Display(void);
void SSD1306_Clear(unsigned dat);


#endif

