#include <stdio.h>
#include <iot_adc.h>
#include "ebox_gas_mq7.h"

/* MQ7传感器ADC通道号 */
#define MQ7_ADC_IDX_06  6

#define ADC_BUF_LENGTH  128
u16 s_adc_buf[ADC_BUF_LENGTH] = { 0 };

/* 将内部值转换成电压值，取多次采样的最高电压值 */
float Mq7ValueToVoltage(u32 data_len)
{
    u32 i;
    float vlt_max = 0;
    u16 vlt = 0;
    /* 循环处理采集得到的数据，对于返回的每一个16位数据，首先根据公式转换成电压值，然后取所有数据中最大值 */
    for(i = 0;i < data_len;i++)
    {
        vlt = s_adc_buf[i];
        float voltage = (float)vlt * 1.8 * 4 / 4096.0;
        vlt_max = (voltage > vlt_max) ? voltage:vlt_max;
    }
    /* 公式：vlt * 1.8 * 4 / 4096.0，将内部值转换成电压值 */
    

    return vlt_max;
}

/* 采集可燃气体传感器的电压，并返回本阶段采到的最大值 */
u32 Mq7GatherVoltage(float *val)
{
    u32 ret = 0, i = 0;
    u16 data = 0;
    float vlt_max = 0;

    memset_s(s_adc_buf, sizeof(s_adc_buf), 0x0, sizeof(s_adc_buf));
	for (u8 em = 0; em < IOT_ADC_EQU_MODEL_BUTT; em++) 
    {
        /* 从ADC通道读取可燃气体传感器采集的数据，保存到s_adc_buf，然后获取最大值并返回给vlt_max */
        for(i = 0;i < ADC_BUF_LENGTH; i ++)
        {
            ret = IoTAdcRead(MQ7_ADC_IDX_06,&data,(IotAdcEquModelSel)em,IOT_ADC_CUR_BAIS_DEFAULT,0);
            if(ret != KP_ERR_SUCCESS)
            {
                printf("IoTAdcRead Fail,ret:#%x\n",ret);
                return KP_ERR_SUCCESS;
            }
            s_adc_buf[i] = data;
        }
        float voltage = Mq7ValueToVoltage(ADC_BUF_LENGTH);
        vlt_max = (voltage > vlt_max) ? voltage:vlt_max;
    }
    *val = vlt_max;
	return KP_ERR_SUCCESS;
}

