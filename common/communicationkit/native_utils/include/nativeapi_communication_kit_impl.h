/*
 * Copyright (c) 2020 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef JS_FS_COMMUNICATION_KIT_IMPL_API_H
#define JS_FS_COMMUNICATION_KIT_IMPL_API_H

#ifdef __cplusplus
#if __cplusplus
extern "C" {
#endif
#endif /* __cplusplus */

#define KEY_MAX_LEN 32
#define VALUE_MAX_LEN 128


#define LOG_TAG "COMMUNICATION_KIT"
#include "hilog/hiview_log.h"

#define LOG_E(fmt, ...) HILOG_ERROR(HILOG_MODULE_APP, fmt, ##__VA_ARGS__)
#define LOG_I(fmt, ...) HILOG_INFO(HILOG_MODULE_APP, fmt, ##__VA_ARGS__)


typedef int (*JsCallClangFunctionDef)(const char *key, char *value);

void JsGetClangFuncationRegister(JsCallClangFunctionDef jsGetClang);
void JsSetClangFuncationRegister(JsCallClangFunctionDef jsSetClang);
void JsGetClangFuncationUnregister(void);
void JsSetClangFuncationUnregister(void);

int GetCommunicationKitValue(const char* key, char* value);
int SetCommunicationKitValue(const char* key, const char* value);

#ifdef __cplusplus
#if __cplusplus
}
#endif
#endif /* __cplusplus */

#endif /* JS_FS_COMMUNICATION_KIT_IMPL_API_H */