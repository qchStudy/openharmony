#ifndef __OLED_TEST_H__
#define __OLED_TEST_H__

#include <_ansi.h>

_BEGIN_STD_C

//--------------OLED参数定义---------------------
#define PAGE_SIZE   8
#define XLevelL     0x02 //1.3寸XLevelL为0x02 0.9寸XLevelL为0x00
#define XLevelH     0x10
#define YLevel      0xB0
#define	Brightness  0xFF 
#define WIDTH       128
#define HEIGHT      64

void OledTestBorder(void);
void OledTestFonts(void);
void OledTestFPS(void);
void OledTestAll(void);
void OledTestLine(void);
void OledTestRectangle(void);
void OledTestCircle(void);
void OledTestArc(void);
void OledTestPolyline(void);
void OledMenu2(float temperature, float humidity);


_END_STD_C

#endif // __OLED_TEST_H__