#ifndef __EBOX_GAS_MQ7_H__
#define __EBOX_GAS_MQ7_H__

#include "kp_base_type.h"


/* 采集可燃气体传感器的电压，并返回本阶段采到的最大值 */
u32 Mq7GatherVoltage(float *val);

#endif

