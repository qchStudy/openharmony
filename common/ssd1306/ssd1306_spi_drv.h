#ifndef __SSD1306_SPI_DRV_H__
#define __SSD1306_SPI_DRV_H__

#include "iot_gpio.h"
#include "wifiiot_gpio.h"
#include "iot_spi.h"
#include "iot_base_type.h"

//--------------SSD1306参数定义---------------------
#define PAGE_SIZE    8
#define XLevelL      0x02 //1.3寸XLevelL为0x02 0.9寸XLevelL为0x00
#define XLevelH      0x10
#define YLevel       0xB0
#define	Brightness   0xFF 
#define WIDTH        128
#define HEIGHT       64

//-------------SSD1306写命令和数据定义-------------------
#define SSD1306_CMD     0  //D/CX为低电平时写命令
#define SSD1306_DATA    1  //D/CX为高电平时写数据

//-----------------SSD1306端口操作定义---------------- 
#if 1
#define SSD1306_CS_Clr(...) IoTGpioSetOutputVal(WIFI_IOT_GPIO_IDX_12, IOT_GPIO_VALUE0, ##__VA_ARGS__)
#define SSD1306_CS_Set(...) IoTGpioSetOutputVal(WIFI_IOT_GPIO_IDX_12, IOT_GPIO_VALUE1, ##__VA_ARGS__)

#define SSD1306_DC_Clr(...) IoTGpioSetOutputVal(WIFI_IOT_GPIO_IDX_11, IOT_GPIO_VALUE0, ##__VA_ARGS__)
#define SSD1306_DC_Set(...) IoTGpioSetOutputVal(WIFI_IOT_GPIO_IDX_11, IOT_GPIO_VALUE1, ##__VA_ARGS__)

#define SSD1306_RST_Clr(...)  IoTGpioSetOutputVal(WIFI_IOT_GPIO_IDX_8, IOT_GPIO_VALUE0, ##__VA_ARGS__)
#define SSD1306_RST_Set(...)  IoTGpioSetOutputVal(WIFI_IOT_GPIO_IDX_8, IOT_GPIO_VALUE1, ##__VA_ARGS__)
#else
#define OLED_CS_Clr() hi_gpio_set_ouput_val(HI_GPIO_IDX_12,HI_GPIO_VALUE0);
#define OLED_CS_Set() hi_gpio_set_ouput_val(HI_GPIO_IDX_12,HI_GPIO_VALUE1);

#define OLED_DC_Clr()  hi_gpio_set_ouput_val(HI_GPIO_IDX_11,HI_GPIO_VALUE0);
#define OLED_DC_Set()  hi_gpio_set_ouput_val(HI_GPIO_IDX_11,HI_GPIO_VALUE1);

#define OLED_RST_Clr()  hi_gpio_set_ouput_val(HI_GPIO_IDX_8,HI_GPIO_VALUE0);
#define OLED_RST_Set()  hi_gpio_set_ouput_val(HI_GPIO_IDX_8,HI_GPIO_VALUE1);

#define SPI_WriteByte(SPI2,dat) SpiWriteData(0,dat);
#endif

//SSD1306初始化
void SSD1306InitGPIO(void);
void SSD1306InitIO(u8 id);


//SSD1306控制用函数
void SSD1306Reset(void);
u32 SSD1306WriteBuf(u32 id, u8 *data, u32 byteLen);
u32 SSD1306WriteCmd(u32 id, u8 cmd);
#define SSD1306WRCmd(...) SSD1306WriteCmd(IOT_SPI_ID_0, ##__VA_ARGS__)
u32 SSD1306WriteData(u32 id, u8 *data, u32 byteLen);
#define SSD1306WRData(...) SSD1306WriteData(IOT_SPI_ID_0, ##__VA_ARGS__)
void SSD1306Init(void);
void SSD1306SetPos(unsigned char x, unsigned char y);
void SSD1306DisplayOn(void);
void SSD1306DisplayOff(void);
void SSD1306SetPixel(unsigned char x, unsigned char y,unsigned char color);
void SSD1306Display(void);
void SSD1306Clear(unsigned dat);


#endif

