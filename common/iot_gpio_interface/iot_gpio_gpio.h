#ifndef __IOT_GPIO_GPIO_H__
#define __IOT_GPIO_GPIO_H__

#include "iot_base_type.h"


/* GPIO初始化复用为GPIO */
u32 GpioInitGpio(u32 pin);

//通过输出高低电平控制GPIO
u32 GpioCtrlGpio(u32 pin, u32 state);


#endif

