#ifndef __IOT_BASE_TYPE_H__
#define __IOT_BASE_TYPE_H__

#include <stdio.h>
#include <unistd.h>

/* 基本数据类型 */
typedef unsigned char           u8;
typedef unsigned short          u16;
typedef unsigned int            u32;
typedef unsigned long long      u64;
typedef unsigned long           ulong;
typedef char                    s8;
typedef short                   s16;
typedef int                     s32;
typedef long long               s64;
typedef long                    slong;

#undef ERROR
#define ERROR (-1)

#define KP_ERR_SUCCESS  0
#define KP_ERR_FAILURE  (-1)

#ifndef NULL
#define NULL ((void *)0)
#endif

#define IOT_U8_MAX             0xFF
#define IOT_U16_MAX            0xFFFF
#define IOT_U32_MAX            0xFFFFFFFF
#define IOT_U64_MAX            0xFFFFFFFFFFFFFFFFUL

#define msleep(x)               usleep((x)*1000)

typedef enum {
    GPIO_OUT_LOW = 0,      /* 低电平 */
    GPIO_OUT_HIGH = 1          /* 高电平 */
} gpio_out_value;

typedef enum {
    /** Physical port 0 */
    IOT_UART_IDX_0,
    /** Physical port 1 */
    IOT_UART_IDX_1,
    /** Physical port 2 */
    IOT_UART_IDX_2,
    /** Maximum value */
    IOT_UART_IDX_MAX
}uart_port_idx;


typedef enum {
    PWM_PORT_PWM0 = 0, /* PWM0端口 */
    PWM_PORT_PWM1 = 1, /* PWM1端口 */
    PWM_PORT_PWM2 = 2, /* PWM2端口 */
    PWM_PORT_PWM3 = 3, /* PWM3端口 */
    PWM_PORT_PWM4 = 4, /* PWM4端口 */
    PWM_PORT_PWM5 = 5, /* PWM5端口 */
    PWM_PORT_MAX   /* 最大值，不可使用 */
} pwm_port;

typedef enum {
    PWM_OUT_STOP  = 0,      /* PWM停止 */
    PWM_OUT_START = 1       /* PWM启动 */
} pwm_out_value;

typedef enum {
    /** I2C hardware index 0 */
    IOT_I2C_IDX_0,
    /** I2C hardware index 1 */
    IOT_I2C_IDX_1,
} i2c_port_idx;


typedef enum {
    LED_OFF = 0,
    LED_ON = 1,
    LED_SPARK = 2
} led_state;
#define LED_SPARK_INTERVAL_TIME 30

#define IOT_DEBUG(fmt, args...)  do {                \
        printf("[IOT][%s][%d]", __FILE__, __LINE__); \
        printf(fmt, ##args);                        \
        printf("\r\n");                             \
    } while (0)

#define IOT_PRINTF_FUNC(fmt, args...)  do {         \
        printf("[%s]", __FUNCTION__); \
        printf(fmt, ##args);                        \
        printf("\r\n");                             \
    } while (0)



#endif
