#include <stdio.h>
//#include <string.h>
#include <unistd.h>
#include "ohos_init.h"
#include "cmsis_os2.h"


void thread1(void)
{
    int sum = 0;
    while (1)
    {
        printf("This is thread1----%d\r\n", sum++);
        usleep(1000000);
    }
}


void thread2(void)
{
    int sum = 0;
    while (1)
    {
        printf("This is thread2----%d\r\n", sum++);
        usleep(500000);
    }
}


static void Thread_sample(void)
{
    osThreadAttr_t attr;

    attr.name = "thread1";
    attr.attr_bits = 0U;
    attr.cb_mem = NULL;
    attr.cb_size = 0U;
    attr.stack_mem = NULL;
    attr.stack_size = 1024 * 4;
    attr.priority = 25;

    osThreadId_t threadID1 = osThreadNew((osThreadFunc_t)thread1, NULL, &attr);
    if (threadID1 == NULL)
    {
        printf("Falied to create thread1!\n");
    }

    attr.name = "thread2";

    osThreadId_t threadID2 = osThreadNew((osThreadFunc_t)thread2, NULL, &attr);
    if (threadID2 == NULL)
    {
        printf("Falied to create thread2!\n");
    }
    
    int sum = 0;
    usleep(1000000);
    osStatus_t status = osThreadSuspend(threadID1);
    printf("This is main thread: suspend thread1, result:%d\r\n", status);
    
    for (int i = 0; i < 5; i++)
    {
        printf("This is main thread----%d\r\n", sum++);
        usleep(1000000);
    }

    status = osThreadResume(threadID1);
    printf("This is main thread: resume thread1, result:%d\r\n", status);

    usleep(1000000);
    status = osThreadTerminate(threadID1);
    printf("This is main thread: terminate thread1, result:%d\r\n", status);
    status = osThreadTerminate(threadID2);
    printf("This is main thread: terminate thread2, result:%d\r\n", status);

    while (1)
    {
        printf("This is main thread----%d\r\n", sum++);
        usleep(1000000);
    }
}

APP_FEATURE_INIT(Thread_sample);
