
/*
 * Copyright (c) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <unistd.h>
#include "lwip/ip_addr.h"
#include "wifi_hotspot.h"
#include "wifi_hotspot_config.h"
#include "iot_softap_xradio.h"
#include "iot_demo_def.h"

#define    DEFAULT_AP_NAME    "TestIotAP"

int BOARD_SoftApStart(const char *ap_name)
{
    HotspotConfig config = {0};
    char *apName = ap_name;
    int retval;

    if (IsHotspotActive() == WIFI_HOTSPOT_ACTIVE) {
        RaiseLog(LOG_LEVEL_ERR, "WIFI_HOTSPOT_ACTIVE \n");
        return -1;
    }
    if (apName == NULL) {
        apName = DEFAULT_AP_NAME;
    }
    if (strcpy_s(config.ssid, sizeof(config.ssid), apName) != 0) {
        RaiseLog(LOG_LEVEL_ERR, "[sample] strcpy ssid fail.\n");
        return -1;
    }

    config.band = HOTSPOT_BAND_TYPE_2G;
    config.securityType = WIFI_SEC_TYPE_OPEN;
    retval = SetHotspotConfig((const HotspotConfig *)(&config));
    if (retval != WIFI_SUCCESS) {
        RaiseLog(LOG_LEVEL_ERR, "SetHotspotConfig \n");
        return -1;
    }

    retval = EnableHotspot();
    if (retval != WIFI_SUCCESS) {
        RaiseLog(LOG_LEVEL_ERR, "EnableHotspot failed! \n");
        return -1;
    }

    return 0;
}

void BOARD_SoftApStop(void)
{
    if (IsHotspotActive() == WIFI_HOTSPOT_NOT_ACTIVE) {
        RaiseLog(LOG_LEVEL_ERR, "WIFI_HOTSPOT_NOT_ACTIVE \n");
        return;
    }

    DisableHotspot();
}
