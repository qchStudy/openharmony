#include <stdio.h>  // for printf
#include <string.h> // for memory operation
#include <stdlib.h>
#include <unistd.h>  // for sleep
#include "ohos_init.h"
#include "cmsis_os2.h"
#include "lwip/netifapi.h"
#include "lwip/api_shell.h"
#include "lwip/sockets.h"
#include "wifi_sta.h"
#include <hi_early_debug.h>
#include "hi_task.h"

#define CONFIG_WIFI_SSID "yy" // 修改为自己的WiFi 热点账号
#define CONFIG_WIFI_PWD "hjy020225" // 修改为自己的WiFi 热点密码

#define TCP_LISTEN_PORT           5555 // TCP服务器监听端口
#define IP_TCP_SERVER_LISTEN_NUM  4  // TCP最大连接数

static int g_listen_fd = 0;
int client_fd = 0;
char recvbuf[512] = {0};
const char *qihangsendmsg = "Hello! Received your message. This is TCP Server!";
static unsigned int Test;//任务id


/* 按照注释要求补全函数功能代码 */
/*任务执行函数，启动TCP服务器端*/
static int TcpServerTask() {
    struct sockaddr_in srv_addr = {0};
    struct sockaddr_in client_addr = {0};
    int ret = 0;
    unsigned int opt = 1;

    /* 连接wifi热点 */
    WifiConnect(CONFIG_WIFI_SSID,CONFIG_WIFI_PWD);
    sleep(2);

    /* 创建socket，保存到g_listen_fd，并打印结果 */
    g_listen_fd = socket(AF_INET, SOCK_STREAM, 0);
    if(g_listen_fd == -1)
    {
        printf("tcp_server_task: create socket failed!\r\n");
        return -1;
    }else{
        printf("tcp_server_task: create socket success!\r\n");

    }

    /* 设置g_listen_fd的socket参数，并打印结果 */
    ret = setsockopt(g_listen_fd,SOL_SOCKET,SO_REUSEADDR, &opt, sizeof(opt));
    if(ret == -1)
    {
        printf("tcp_server_task: set socket option failed!\r\n");
        return -1;
    }else{
        printf("tcp_server_task: set socket option success!\r\n");
    }

    /* 绑定g_listen_fd的监听端口，并打印结果 */
    srv_addr.sin_family = AF_INET;
    srv_addr.sin_addr.s_addr = htonl(INADDR_ANY);
    srv_addr.sin_port = htons(TCP_LISTEN_PORT);
    ret = bind(g_listen_fd,(struct sockaddr *)&srv_addr,sizeof(srv_addr));
    if(ret != 0)
    {
        printf("tcp_server_task:bind failed,return is %d\r\n",ret);
        closesocket(g_listen_fd);
        return -1;
    }
    else{
        printf("tcp_server_task:bind success!\r\n");
    }

    /* 开始监听端口，并打印结果 */
    ret = listen(g_listen_fd,IP_TCP_SERVER_LISTEN_NUM);
    if(ret != 0)
    {
        printf("tcp_server_task:listen failed,return is %d\r\n",ret);
        closesocket(g_listen_fd);       
        g_listen_fd = -1;
        return -1;
    }else{
        printf("tcp_server_task:listen success!\r\n");
    }

    /* 循环处理客户端连接请求
    1、当有客户端请求连接时，新建一个socket与客户端连接
    2、通过新建的socket从客户端接收数据
    3、通过新建的socket发送数据给客户端
    */
    while(1)
    {
        int sin_size = sizeof(struct sockaddr_in);
        if((client_fd = accept(g_listen_fd, (struct sockaddr_in *)&client_addr,(socklen_t *)&sin_size)) == -1)
        {
            printf("tcp_server_task:accept failed!\r\n");
            continue;
        }
        printf("tcp_server_task:accept client addr: %s\r\n",inet_ntoa(client_addr.sin_addr));
        ssize_t ret = 0;
        while(1)
        {
            sleep(2);
            if((ret = recv(client_fd,recvbuf,sizeof(recvbuf), 0)) == -1)
            {
                printf("tcp_server_task:recv failed!\r\n");
                break;
            }else{
                printf("recv :%s\r\n",recvbuf);
            }
            sleep(2);
            if((ret = send(client_fd,qihangsendmsg,strlen(qihangsendmsg) + 1 ,0)) == -1)
            {
                printf("tcp_server_task:send failed!\r\n");
                break;
            }else{
                printf("send :%s\r\n",qihangsendmsg);
            }
        }
    }

    closesocket(g_listen_fd);
    g_listen_fd = -1;

    return 0;
}

void TcpServerSample()
{
    osThreadAttr_t attr;
    attr.name = "TcpServerTask";
    attr.attr_bits = 0U;
    attr.cb_mem = NULL;
    attr.cb_size = 0U;
    attr.stack_mem = NULL;
    attr.stack_size = 4096;
    attr.priority = 20;
    if (osThreadNew((osThreadFunc_t)TcpServerTask, NULL, &attr) == NULL) {
        printf("Failed to create TcpServerTask!\n");
    }
}

APP_FEATURE_INIT(TcpServerSample);
