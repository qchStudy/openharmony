#ifndef __EBOX_MANHOLE_LED_7_H__
#define __EBOX_MANHOLE_LED_7_H__

#include "kp_base_type.h"


//初始化烟雾探测模块的LED报警灯
void ManholeLedInit(void);

//控制烟雾探测模块的LED报警灯，1:点亮; 2:熄灭
void ManholeLedCtrl(u32 state);


#endif

