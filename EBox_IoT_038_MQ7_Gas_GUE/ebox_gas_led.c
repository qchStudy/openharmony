#include "iot_gpio.h"
#include "iot_gpio_ex.h"
#include "kp_base_type.h"
#include "ebox_gas_led.h"
#define LED_OFF 0
#define LED_ON 1
#define LED_IO_GPIO_05  5


/* 初始化LED */
void LedInit(void)
{
	/* LED报警灯连接芯片的GPIO5引脚，初始化GPIO-05引脚 */
    int ret = IoTGpioInit(LED_IO_GPIO_05);
    if (ret != 0)
    {
        printf("IoTGpioInit failed :%#x \r\n", ret);
        return;
    }

	/* 设置复用为GPIO功能 */
	ret = IoTGpioSetFunc(LED_IO_GPIO_05, IOT_GPIO_FUNC_GPIO_5_GPIO);
    if (ret != 0)
    {
        printf("IoTGpioSetFunc failed :%#x \r\n", ret);
        return;
    }

	/* 设置成输出有效，通过输出高电平点亮LED灯 */
	ret = IoTGpioSetDir(LED_IO_GPIO_05, IOT_GPIO_DIR_OUT);
    if (ret != 0)
    {
        printf("IoTGpioSetDir failed :%#x \r\n", ret);
        return;
    }
    printf("----- LedGpioInit success! -----\r\n");
	
	printf("LedInit success! \r\n");
}

/* 点亮或熄灭LED灯 */
void LedCtrl(u32 state)
{
	/* 根据传入的参数来控制LED点亮、熄灭或闪烁 */
	switch (state)
    {
    case LED_ON:
        /* code */IoTGpioSetOutputVal(LED_IO_GPIO_05, LED_ON);
        break;
    
    default:
        IoTGpioSetOutputVal(LED_IO_GPIO_05, LED_OFF);
        break;
    }

	
}

