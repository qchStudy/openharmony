#include <stdio.h>
#include <cmsis_os2.h>
#include "ohos_init.h"
#include "ebox_gas_mq7.h"
#include "ebox_gas_led.h"
#define LED_OFF 0
#define LED_ON 1
/* 使用PWM驱动蜂鸣器发声，可以通过占空比来调整音量，目前暂时固化成50%占空比 */
#define BUZZER_PWM_DUTY 50
#define TASK_DELAY 200


/* 可燃气体传感器处理任务，周期检测可燃气体电压数据 */
void GasModuleTask(void)
{
    float vlt = 0;
    static int smoke = 0;
	/* 10秒后可燃气体传感器才能正常检测 */
	osDelay(1000);
    while(1){
        u32 ret = Mq7GatherVoltage(&vlt);
		if (ret != KP_ERR_SUCCESS) {
			KP_DEBUG("Mq7GatherVoltage failed");
			continue;
		}
        printf("vlotage is %.3f\r\n", vlt);
        //为了实验安全起见，建议此值不要设置过高
        //可以比正常环境略高即可，即低浓度烟雾也可以触发报警
        if (vlt > 0.4)
        {
            /* 检测到可燃气体，如果之前没有报警，则点亮LED灯，如果之前已经点亮了，则不再点亮 */
            LedCtrl(LED_ON);
            
        } else {
            /* 未检测到可燃气体，如果之前有报警，则熄灭LED灯，如果之前已经熄灭了，则不再熄灭 */
            LedCtrl(LED_OFF);
		    
        }

        //控制检测频率，目前暂定2秒一次
        osDelay(TASK_DELAY);
    }
    return;
}

void GasModuleSample(void)
{
    LedInit();
    
	/* 设置线程属性 */
    osThreadAttr_t attr;
    attr.name = "GasModuleTask";
    attr.attr_bits = 0U;
    attr.cb_mem = NULL;
    attr.cb_size = 0U;
    attr.stack_mem = NULL;
    attr.stack_size = 1024 * 4;
    attr.priority = 20;

    /* 创建烟雾传感器处理任务 */
    if (osThreadNew((osThreadFunc_t)GasModuleTask, NULL, &attr) == NULL)
    {
        printf("Falied to create GasModuleTask!\r\n");
    }
}

APP_FEATURE_INIT(GasModuleSample);

